/*global _,document,Backbone,$,Spinner,news*/
var keyword = 'target_birthday2015';

var Article = Backbone.Model.extend({
     urlRoot: 'http://www.army.mil/api/packages/getpackagesbykeywords' +
         '?keywords=' + keyword
});

var Articles = Backbone.Collection.extend({
    model: Article,
    url: 'http://www.army.mil/api/packages/getpackagesbykeywords' +
        '?keywords=' + keyword
});

var articles = new Articles();

var ArticleGallery = Backbone.View.extend({
    initialize: function(options) {
        var opts = {
            lines: 13, // The number of lines to draw
            length: 20, // The length of each line
            width: 10, // The line thickness
            radius: 30, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            color: '#000', // #rgb or #rrggbb or array of colors
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%' // Left position relative to parent
        };
        this.spinner = new Spinner(opts).spin();
        // refer to the spinner with: target.append(spinner.el);
        // then stop with spinner.stop();

        $('.news-box .news-overlay').append(this.spinner.el);

        for (var i = 0; i < options.rows; i++) {
            this.listenTo(
                articles, 'sync', _.bind(this.load_articles, this, i)
            );
        }
        articles.url += '&offset=' + options.offset + '&count=' + options.count;
        articles.fetch();
    },
    spinner: '',
    done: function() {
        this.spinner.stop();
        $('.news-box .news-overlay').hide();
        $('.top-stories ul li').last().css(
            'border-bottom', '1px solid #d5d0c8'
        );
        $('.top-stories ul li:lt(4)').addClass('visible');
        $('.top-stories ul li:lt(2)').addClass('mobile-desktop');
        $('.top-stories ul li').filter(':eq(4),:eq(5)')
            .addClass('nav-tablet');
    },
    load_articles: function(idx) {
        var news_stories = $('.top-stories ul'),
        shifted = articles.shift();

        // call load_main and load_tablet here
        if (idx === 0) {
            build_main_article(shifted);
            build_tablet_articles(shifted);
        } else if (idx > 0 && idx < 3) {
            build_tablet_articles(shifted);
            news_stories.append(build_article_list(shifted));
        } else {
            news_stories.append(build_article_list(shifted));
        }
        if (articles.length === 0) this.trigger(this.done());
    }
});

function build_main_article(article) {
    var container = $('.image-container'),
    max_index = article.get('images').length - 1,
    random_index = Math.floor((Math.random() * max_index) + 0),
    image = article.get('images')[random_index].url_size1,
    link = $('<a>'), alt, title, url;

    alt = article.get('images')[random_index].alt;
    title = article.get('title');
    url = article.get('page_url');

    $('<img>').load(function() {
        container.prepend($(this));
        news.init();
    }).prop({
        'class': 'ie',
        'src': image,
        'alt': alt
    });

    container.find('.inner-container .image').css({
        'background-image': 'url(' + image + ')'
    });

    link.prop({
        'href': url,
        'target': '_blank'
    }).html(title);
    container.find('.caption p').append(link);
}

function build_tablet_articles(article) {
    var container = $('.tablet-top-stories'),
    story = $('<div>').prop({'class': 'story'}),
    link = $('<a>'),
    max_index = article.get('images').length - 1,
    random_index = Math.floor((Math.random() * max_index) + 0),
    image = article.get('images')[random_index].url_size2,
    alt, title, url;

    alt = article.get('images')[random_index].alt;
    title = article.get('title');
    url = article.get('page_url');

    $('<img>').load(function() {
        story.prepend($(this));
    }).prop({
        'src': image,
        'alt': alt
    });

    link.prop({
        'href': url,
        'target': '_blank'
    }).html(title);

    story.append(link);

    container.append(story);
}

function build_article_list(article) {
    var news_story = $('<li>'),
    max_index = article.get('images').length - 1,
    random_index = Math.floor((Math.random() * max_index) + 0),
    image = article.get('images')[random_index].url_size3,
    alt, title, url, image_url, title_url, news_story_image, news_story_link;

    alt = article.get('images')[random_index].alt;
    title = article.get('title');

    url = article.get('page_url');

    image_url = url + '?from=ab_240_image';
    title_url = url + '?from=ab_240_text';

    news_story_image = $('<a>').prop({
        'href': image_url,
        'target': '_blank',
        'class': 'img-url'
    });

    $('<img>').load(function() {
        news_story_image.append($(this));
    }).prop({
        'src': image,
        'alt': alt
    });
    news_story_link = $('<a>').prop({
        'class': 'title-url',
        'href': title_url,
        'target': '_blank'
    }).html(title);

    return news_story.append(news_story_image).append(news_story_link);
}

$(document).ready(function() {
    var articleGallery, options = {
        offset: 1,
        count: 100,
        rows: 100
    };

    $('#slides').waypoint(function() {
        // load articles when we scroll down to the slides section
        if (!articleGallery) {
            articleGallery = new ArticleGallery(options);
        }
    }, {
        offset: 'bottom-in-view'
    });

    // bind click function to load more button
    // $('.top-stories .mobile-tablet').click(function() {
        // for (var i = 0; i < 4; i++) {
            // articleGallery.load_articles();
        // }
    // });

});
