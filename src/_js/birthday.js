/*global $, document, window, Modernizr, twttr, FastClick*/
/*jslint white: true*/

var dragging = false,
    hist = {} || hist,
    lightbox = {} || lightbox,
    lightbox_slideshow = [],
    main_slideshow_items = {
        'owl': []
    },
    menu = {} || menu,
    mobile_slideshow = [],
    news = {} || news,
    path = 'http://usarmy.vo.llnwd.net/e2/rv5_images/birthday/2015/',
    position = $(window).scrollTop(),
    products = {} || products,
    slider = {} || slider,
    slideshow_details,
    start_width,
    quiz,
    window_width = window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;

/*
 ***********************************************************************
 * CONTENT TEAM - MAKE YOUR CHANGES HERE
 ***********************************************************************
 */
slideshow_details = {
    'items': [{
        'title': '2015 Army Birthday Ball',
        'cutline': 'From left to right, Army Chief of Staff Gen. Ray Odierno, Secretary of the Army John M. McHugh and Sgt. Maj. of the Army Daniel A Dailey cut the Army Birthday cake during the 2015 Army Ball in Washington, D.C., June 13, 2015. (Photo by Staff Sgt. Steve Cortez)',
        'filename': '240_army_birthday_15.jpg',
        'alt': 'Senior leaders cutting cake at the Army Birthday Ball'
    }, {
        'title': 'Gen. Ray Odierno in Times Square for Army Birthday 2015',
        'cutline': 'U.S. Army Chief of Staff, Gen. Ray Odierno, poses with a Soldier after the U.S. Army Soldier Show on Military Island, Times Square, New York, June 12, 2015. Odierno was visiting New York the week in celebration of the Army\'s 240th birthday, which falls on June 14. (Photo by Staff Sgt. Chuck Burden)',
        'filename': '240_army_birthday_16.jpg',
        'alt': 'General Odierno posing with a Soldier in New York City'
    }, {
        'title': 'Old Guard Readies Wreath',
        'cutline': 'Old Guard Soldiers stand at attention during the playing of the National Anthem, June 14, 2015. Secretary of the Army John McHugh, Army Chief of Staff Gen. Ray Odierno and Sgt. Maj. of the Army Daniel Dailey laid a wreath commemorating the Army\'s 240th birthday at the Tomb of the Unknowns at Arlington National Cemetery on the day of the Army\'s birth, June 14, 1775. (Photo by J.D. Leipold, ARNEWS)',
        'filename': '240_army_birthday_17.jpg',
        'alt': 'Wreath-laying at Arlington National Cemetery'
    }, {
        'title': 'ANAD celebrates Army birthday',
        'cutline': 'Maj. Aatif Hayat, physician for the Dear Occupational Health Clinic at Anniston Army Depot, cuts one of the installation\'s cakes in celebration of the Army\'s 240th Birthday at the installation\'s Children\'s Development Center. (Photo by Mrs. Jennifer Bacchus, AMC)',
        'filename': '240_army_birthday_19.jpg',
        'alt': 'Cake-cutting at Children\'s Development Center'
    }, {
        'title': 'Cake serving',
        'cutline': 'Initial Entry Training Soldiers line up for a piece of cake during the 95th Adjutant General Battalion (Reception) Army birthday celebration, June 12, 2015, at the battalion\'s complex. About 600 of the Army\'s newest Soldiers learned about the history, missions, heritage and their role in the Army during the event. (Photo by Jeff Crawley, Fort Sill Cannoneer)',
        'filename': '240_army_birthday_18.jpg',
        'alt': 'Army birthday celebration at Fort Sill'
    }, {
        'title': '240th Army Birthday Cake-Cutting at Pentagon',
        'cutline': 'The Army\'s oldest active-duty Soldier Dr. (Col.) Arthur Wittich and the youngest Pvt. Cody Clark flank and assist Army Secretary John McHugh and Army Vice Chief Gen. Daniel Allyn slice into the cake at the Army\'s 240th birthday celebration in the Pentagon courtyard June 11. (Photo by J.D. Leipold, ARNEWS)',
        'filename': '240_army_birthday_13.jpg',
        'alt': 'Pentagon cake-cutting ceremony'
    }, {
        'title': 'Army celebrates 240th birthday at U.S. Capitol',
        'cutline': 'U.S. Army Chief of Staff Gen. Ray Odierno, U.S. Sen. Jim Inhofe, Army Secretary John M. McHugh, U.S. Sen. Pat Roberts, Rep. "Dutch" Ruppersberger of Maryland, and Sgt. Maj. of the Army Daniel A. Dailey cut the ceremonial cake during the 240th Army Birthday Capitol Hill cake-cutting ceremony at the U.S. Capitol Visitors\' Atrium in Washington, D.C., June 10, 2015. (Photo by Eboni L. EversonMyart)',
        'filename': '240_army_birthday_14.jpg',
        'alt': 'U.S. Capitol cake-cutting ceremony'
    }, {
        'title': 'McHugh kicks off Army birthday week, presents Purple Hearts',
        'cutline': 'Army Secretary John M. McHugh lays a wreath at the tomb of George Washington at Washington\'s Mount Vernon estate, kicking off events for the Army\'s 240th birthday, in Alexandria, Va., June 9, 2015. (Photo by Ms. Lisa Ferdinando, ARNEWS)',
        'filename': '240_army_birthday_09.jpg',
        'alt': 'Arlington National Cemetery wreath laying'
    }, {
        'title': 'Evaluation, education, compensation dominate discussion at Dailey town hall',
        'cutline': 'Sgt. Maj. of the Army Daniel A. Dailey discusses Soldier evaluation, education and compensation during an Army birthday town hall meeting with Soldiers at Defense Media Activity on Fort Meade, Md., June 4, 2015. (Photo by C. Todd Lopez)',
        'filename': '240_army_birthday_10.jpg',
        'alt': 'Army Birthday Town Hall'
    }, {
        'title': 'U.S. Army celebrates 240th birthday in New York',
        'cutline': 'The Old Guard Fife and Drum Corps perform in Time Square during the Army Birthday Celebration in 2013. The unit is the only one of its kind in the armed forces, and is part of the 3rd U.S. Infantry Regiment (The Old Guard). It is stationed on Joint Base Myer-Henderson Hall in Arlington, Va. (Photo by JFHQ-NCR/MDW Public Affairs Office)',
        'filename': '240_army_birthday_11.jpg',
        'alt': 'Old Guard Fife and Drum Corps'
    }, {
        'title': 'Army leaders to host "We Serve" Soldier Show in D.C.',
        'cutline': 'Spc. Jovan Maires, of Fort Hood, Texas, stands front and center during a performance of the 2015 U.S. Army Soldier Show "We Serve" on Joint Base San Antonio-Fort Sam Houston. Army Chief of Staff Gen. Ray Odierno and Sgt. Maj. of the Army Daniel A. Dailey will host the U.S. Army Soldier Show\'s Army Birthday Week performance at the Warner Theatre in Washington, D.C., June 9, 2015. (Photo by Tim Hipps, IMCOM Public Affairs)',
        'filename': '240_army_birthday_12.jpg',
        'alt': 'U.S. Army Soldier Show'
    }, {
        'title': 'Army National Guard',
        'cutline': 'Salem, Massachusetts, 1637 -- The history of the National Guard began, Dec. 13, 1636, when the General Court of the Massachusetts Bay Colony ordered the organization of the colony\'s militia companies into three regiments: the North, South and East Regiments. The colonists had adopted the English militia system, which obligated all males, between the ages of 16 and 60, to possess arms and participate in the defense of the community. The early colonial militia drilled once a week and provided guard details each evening to sound the alarm in case of attack. The growing threat of the Pequot Indians to the Massachusetts Bay Colony required that the militia be in a high state of readiness. The organization of the North, South and East Regiments increased the efficiency and responsiveness of the militia. Although the exact date is not known, the first muster of the East Regiment took place in Salem, Massachusetts. The National Guard continues its historic mission of providing units for the first-line defense of the nation. The 101st Engineer Battalion, Massachusetts Army National Guard, continues the East Regiment\'s proud heritage of 350 years of service.',
        'filename': '240_army_birthday_01.jpg',
        'alt': 'National Guard birthday'
    }, {
        'title': 'Taking Aim',
        'cutline': 'A young Soldier holds and sights his Garand rifle like an old timer on Fort Knox, Ky., during June 1942. He likes the piece for its fine firing qualities and its rugged, dependable mechanism. (Photo by Alfred T. Palmer/Courtesy of the Library of Congress)',
        'filename': '240_army_birthday_02.jpg',
        'alt': 'Soldier holds his Garand rifle'
    }, {
        'title': 'Army Nurse Corps',
        'cutline': '"You Are Needed Now," a World War II Army poster, encourages women to join the Army Nurses Corps at the Red Cross recruiting office.',
        'filename': '240_army_birthday_03.jpg',
        'png_filename': '240_army_birthday_03.png',
        'alt': 'Army Nurse Corps poster'
    }, {
        'title': 'World War II',
        'cutline': 'Infantrymen of Company "I" await word to advance in pursuit of retreating Japanese forces during World War II. This photo was taken at Stepping Stone Island on the Vella Lavella Island Front, Southwest Pacific, Sept. 13, 1943. Signal Corps Photo: 161-43-4081 (Schuman)',
        'filename': '240_army_birthday_04.jpg',
        'alt': 'Infantrymen of Company I during World War II'
    }, {
        'title': 'D-Day',
        'cutline': 'An assault landing, one of the first waves at Omaha, is shown in the photo from June 6, 1944. The Coast Guard caption identifies the unit as Company E, 16th Infantry, 1st Infantry Division.',
        'filename': '240_army_birthday_05.jpg',
        'alt': 'Assault landing at Omaha on D-Day'
    }, {
        'title': 'Vietnam War',
        'cutline': 'Combat operations are shown at Ia Drang Valley, Vietnam, November 1965. Maj. Bruce P. Crandall\'s UH-1D helicopter climbs skyward after discharging a load of infantrymen on a search and destroy mission.',
        'filename': '240_army_birthday_06.jpg',
        'alt': 'Maj. Crandall\'s UH-1 Huey dispatching infantry in th la Drang operation'
    }, {
        'title': 'Operation Iraqi Freedom',
        'cutline': 'A Soldier, with A Battery, Regimental Fires Squadron, 278th Armored Cavalry Regiment, 13th Sustainment Command (Expeditionary), walks the tarmac of Mosul Airfield at Contingency Operating Base Diamondback, Iraq, June 9, 2010.',
        'filename': '240_army_birthday_07.jpg',
        'alt': 'Soldier walks the Mosul Airfield tarmac'
    }, {
        'title': 'Operation Enduring Freedom',
        'cutline': 'U.S. Army 1st Lt. Shawn Meno of Mangilao provides security for Provincial Reconstruction Team Farah and members of a local Kuchi tribe residing in Bawka District in Farah province, Afghanistan, June 12, 2010. Meno is assigned to the Provincial Reconstruction Team Farah Security Forces Platoon. The team visited three villages to gather information on the needs of the residents and to promote the importance of seeking government assistance to help resolve those needs. The Kuchi Tribe is a nomadic group, where constant travel and new settlement is a way of life. (U.S. Air Force Senior Airman Rylan K. Albright)',
        'filename': '240_army_birthday_08.jpg',
        'alt': '1st Lt. Meno provides security for Provincial Reconstruction Team Farah'
    }]
};
quiz = {
    'items': [{
        'question': 'Putting the welfare of the Nation, the Army, and your subordinates before your own demonstrates which Army value?',
        'choices': [
            'Duty',
            'Loyalty',
            'Selfless Service',
            'Honor'
        ],
        'correct_answer': 2 // 0 based (subtract 1) DO NOT use quotes in the answer here!!!
    }, {
        'question': 'With the birth of the U.S. Army being June 14, 1775, when did the U.S. Army flag originate?',
        'choices': [
            'February 22, 1951',
            'June 14, 1956',
            'July 4, 1763',
            'June 14, 1775'
        ],
        'correct_answer': 1
    }, {
        'question': 'What was the original design of the first U.S. Army flag?',
        'choices': [
            '13 red and white stripes and 50 white 5-point stars in the blue canton',
            '13 red and white stripes and the Union Jack in the canton',
            '15 red and white stripes and 15 white 5-point stars in the blue canton',
            'White silk with yellow fringe, streamers, and a blue central design of the War Office seal'
        ],
        'correct_answer': 3
    }, {
        'question': 'What do the current 188 streamers on the U.S. Army flag represent?',
        'choices': [
            'The streamers on the Army flag represent fallen heroes of the Revolutionary war and the war of 1812.',
            'The streamers on the Army flag denote the campaigns fought by the Army throughout our nation\'s history.',
            'The streamers on the Army flag represent the number of presidents and vice presidents throughout our national history.',
            'None of the above.'
        ],
        'correct_answer': 1
    }, {
        'question': 'During what war was the highest military award for a Soldier, the first Army Medal of Honor, awarded?',
        'choices': [
            'Revolutionary War',
            'American Civil War',
            'World War I',
            'World War II'
        ],
        'correct_answer': 1
    }, {
        'question': 'How many years did the Civil War last?',
        'choices': [
            'One',
            'Two',
            'Four',
            'Six'
        ],
        'correct_answer': 2
    }, {
        'question': 'When was the Women\'s Army Corps allowed to serve in the regular Army?',
        'choices': [
            '1946',
            '1960',
            '1972',
            '1985'
        ],
        'correct_answer': 0
    }, {
        'question': 'Approximately, what percentage of the Army is comprised of women?',
        'choices': [
            '10',
            '12',
            '16',
            '33'
        ],
        'correct_answer': 2
    }, {
        'question': 'Which unit of the United States Army is commanded by a lieutenant colonel?',
        'choices': [
            'Platoon',
            'Company',
            'Battalion',
            'Brigade'
        ],
        'correct_answer': 2
    }, {
        'question': 'What light utility vehicle replaced the Army Jeep?',
        'choices': [
            'Stryker',
            'Humvee',
            'The Crusader',
            'HEMTT'
        ],
        'correct_answer': 1
    }, {
        'question': 'What military award, designated as the Badge of Military Merit, was established by George Washington-then the commander-in-chief of the Continental Army on August 7, 1782?',
        'choices': [
            'Silver Star',
            'Distinguished Service Cross',
            'Medal of Honor',
            'Purple Heart'
        ],
        'correct_answer': 3
    }, {
        'question': '"Doughboy" refers to Soldiers who served in which Army campaigns?',
        'choices': [
            'The War of 1812',
            'World War I',
            'Vietnam',
            'Desert Storm'
        ],
        'correct_answer': 1
    }, {
        'question': 'An action performed without any expectation of reward is an example of:',
        'choices': [
            'Courage',
            'Integrity',
            'Respect',
            'Selfless Service'
        ],
        'correct_answer': 3
    }, {
        'question': 'What is the minimum age to join the Army?',
        'choices': [
            '16, 15 with parental consent',
            '17, 16 with parental consent',
            '18, 17 with parental consent',
            '19, 18 with parental consent'
        ],
        'correct_answer': 2
    }, {
        'question': 'True or False. The Reserve component of the Army (Guard/Reserve) is larger than the Active Army.',
        'choices': [
            'True',
            'False'
        ],
        'correct_answer': 0
    }]
};
/*
 ***********************************************************************
 * CONTENT TEAM - DO NOT MAKE CHANGES BELOW HERE
 ***********************************************************************
 */

menu = {
    is_open: false,
    flex_open: false,

    show_menu: function() {
        var top_val;
        if (menu.is_open) {
            $('body').css('overflow-y', 'auto');
            $('.menu-list').css('top', '');
            $('.fixed-menu span').removeClass();
            menu.is_open = false;
        } else {
            $('body').css('overflow-y', 'hidden');
            top_val = parseInt($('.fixed-menu').css('top'), 10);
            $('.menu-list').css('top', top_val + 50);
            $('.fixed-menu span').addClass('up');
            menu.is_open = true;
        }
    },

    show_flex: function() {
        if (menu.flex_open) {
            $('.flex-fixed-menu span').removeClass('up');
            $('.flex-list').removeClass('vis');
            menu.flex_open = false;
        } else {
            $('.flex-fixed-menu span').addClass('up');
            $('.flex-list').addClass('vis');
            menu.flex_open = true;
        }
    },

    menu_class: function(obj) {
        $(obj).parent().siblings().removeClass();
        $(obj).parent().addClass('active');
    },

    fix_placement: function() {
        var main_h = $('.main').height(),
            offset = $(window).scrollTop(),
            start_top = parseInt($('.top-menu').outerHeight(true), 10),
            new_top = start_top - offset <= 0 ? 0 : start_top - offset,
            menu_h = $('.fixed-menu').outerHeight(true),
            menu_list_h = $('.fixed-menu').is(':visible') ?
                new_top + menu_h :
                new_top;

        $('.secondary-side-menu').height(main_h);

        $('.fixed-menu').css('top', new_top);

        if (window_width > 699 && window_width < 993) {
            $('.menu-list').css('top', menu_list_h);
        } else {
            $('.menu-list').css('top', '');
        }
    },

    adjust_flex_menu: function() {
        if ($(window).height() < 670) {
            $('.bday-logo').addClass('flex-logo');
            $('.primary-side-menu ul').addClass('flex-list');
            $('.flex-fixed-menu').show();
        } else {
            $('.bday-logo').removeClass('flex-logo');
            $('.primary-side-menu ul').removeClass('flex-list');
            $('.flex-fixed-menu').hide();
        }
    },

    bind_events: function() {
        // var click_handle = Modernizr.touch ? 'touchend' : 'click';

        $('.menu-list ul li a,.primary-side-menu ul li a')
            .on('click', function(e) {
                var target = $(this.hash),
                    off_set = 25;
                e.preventDefault();

                menu.menu_class($(this));

                target = target.length ?
                    target :
                    $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    if (target.prop('id') == 'history') {

                        target = $('.full-width-image-container');
                        off_set = 0;
                    }
                    if (target.offset().top < position && window_width <= 699) {
                        off_set += $('.fixed-menu').outerHeight(true);
                    }
                    $('html,body').animate({
                        scrollTop: target.offset().top - off_set
                    }, 1000);
                    if (window_width <= 699) {
                        menu.show_menu();
                    } else if ($(window).height() < 670 && window_width > 699) {
                        menu.show_flex();
                    }
                }
            });

        $('.fixed-menu').on('click', function(e) {
            if (dragging) return;
            if (window_width <= 699 && e.target.tagName != 'A') {
                menu.show_menu();
            }
        });

        $('.flex-fixed-menu').on('click', function() {
            if (dragging) return;
            menu.show_flex();
        });
    }
};

news = {
    init: function() {
        news.story_box_size();
        news.bind_events();
    },

    story_box_size: function() {
        // i hate to do a js solution for this, but the css is killing me
        var height = $('.top-stories p').outerHeight(true),
            totalheight = $('.news-box').outerHeight(true),
            mq_capability = Modernizr.mq('only all');

        if (window_width > 992 || !mq_capability) {
            $('.top-stories ul').css({
                'height': totalheight - height,
                'top': height - 1 // hide that 1px border-top on the first one
            });
        } else {
            $('.top-stories ul').css({
                'height': '',
                'top': ''
            });
        }
    },

    bind_events: function() {
        // var click_handle = Modernizr.touch ? 'touchend' : 'click';
        var i, stories;

        $('.top-stories .mobile-tablet').on('click', function() {
            if (dragging) return;

            stories = $('.top-stories ul li').not('.visible');

            for (i = 0; i < stories.length && i < 4; i++) {
                $(stories[i]).addClass('visible');
            }

            if (!$('.top-stories ul li').not('.visible').length) {
                $(this).hide();
            }
        });
    }
};

hist = {
    bind_events: function() {
        // var click_handle = Modernizr.touch ? 'touchend' : 'click';
        $('.army-profession .mobile-tablet').on('click', function() {
            if (dragging) return;
            $('.desktop-only, .inner-wrap-right.last').css('display', 'block');
            $(this).hide();
        });
    }
};

lightbox = {
    init: function() {
        var // click_handle = Modernizr.touch ? 'touchend' : 'click',
        lb = $('<div>').prop({
            'id': 'lightbox'
        }),
            outer_container = $('<div>').prop({
                'class': 'outer-container'
            }),
            inner_container = $('<div>').prop({
                'class': 'inner-container'
            }),
            header_wrap = $('<div>').prop({
                'class': 'lightbox-header-wrap'
            }),
            header_content = $('<div>').prop({
                'class': 'lightbox-header'
            }),
            outer_button_wrap = $('<div>').prop({
                'class': 'lightbox-button-wrap lightbox-button-wrap-right ' +
                    'lightbox-close-wrap'
            }),
            inner_button_wrap = $('<div>').prop({
                'class': 'lightbox-buttons top-lightbox-buttons'
            }),
            button_wrap = $('<div>').prop({
                'class': 'lightbox-button'
            }).on('click', function() {
                if (dragging) return;
                lightbox.lb_close();
            }),
            button = $('<div>').prop({
                'class': 'button-icon',
                'id': 'button-icon-close'
            });

        header_wrap.append(
            header_content.append(
                outer_button_wrap.append(
                    inner_button_wrap.append(
                        button_wrap.append(
                            button
                        )
                    )
                )
            )
        );

        outer_container.append(header_wrap).append(inner_container);

        lb.click(function() {
            lightbox.lb_close();
        });
        $('.primary-side-menu, .secondary-side-menu').click(function() {
            lightbox.lb_close();
        });

        outer_container.click(function(e) {
            if (e.target.tagName != 'A' || e.target.className != 'download') {
                e.stopPropagation();
            }
        });

        $('html').css('overflow', 'hidden');
        $('body').append(lb.append(outer_container));
    },

    add_slideshow: function(content) {
        var lb_content = $('<div>').prop({
            'class': 'owl-container'
        }),
            header_content = $('.lightbox-header'),
            lb_content_data,
            caption = $('<div>').prop({
                'class': 'caption'
            });

        header_content.prepend(
            $('<div>').prop({
                'class': 'inner-wrap'
            }).append(
                $('<h2>').html(
                    'Image <span class="img-idx"></span> of ' +
                    '<span class="total-imgs"></span>'
                )
            )
        );

        caption.append(
            $('<div>').prop({
                'class': 'padded-container'
            }).append(
                $('<h3>').prop({
                    'class': 'img-title'
                }),
                $('<p>').prop({
                    'class': 'img-cutline'
                }),
                $('<p>').prop({
                    'class': 'img-download'
                }).append(
                    $('<a>').text('Download Original').append(
                        $('<span>').prop({
                            'class': 'download'
                        })
                    )
                ),
                $('<div>').prop({
                    'class': 'clearboth'
                })
            )
        );

        $('#lightbox .inner-container').append(lb_content, caption);

        lb_content.owlCarousel({
            jsonPath: {
                'owl': lightbox_slideshow
            },
            singleItem: true,
            lazyLoad: true,
            pagination: false,
            navigation: true,
            navigationText: ['', ''],
            responsive: true,
            responsiveBaseWidth: '#lightbox',
            afterAction: function() {
                var item_idx = this.currentItem + 1, // 0 based
                    item_tot = this.itemsAmount;

                $('.img-idx').text(item_idx);
                $('.total-imgs').text(item_tot);

                $('#lightbox .caption h3')
                    .text(slideshow_details.items[this.currentItem].title);
                $('#lightbox p.img-cutline')
                    .text(slideshow_details.items[this.currentItem].cutline),
                $('#lightbox p.img-download a').prop({
                    'href': path + 'original/' +
                        slideshow_details.items[this.currentItem].filename,
                    'target': '_blank'
                });
            }
        });

        lightbox.caption_events();

        // get carousel instance data and jump to the selected index
        lb_content_data = lb_content.data('owlCarousel');
        lb_content_data.jumpTo(content.idx);
    },

    add_mobile_slideshow: function(content) {
        var lb = $('#lightbox'),
            lb_content = $('<div>').prop({
                'class': 'owl-container'
            }),
            header_content = $('.lightbox-header'),
            i = 0; // set i to 0

        header_content.prepend(
            $('<div>').prop({
                'class': 'inner-wrap'
            }).append(
                $('<h2>').text('U.S. Army Birthday Images')
            )
        );

        lb.find('.inner-container').append(lb_content);

        for (i; i < mobile_slideshow.length; i++) {

            /**
             * need to load images to browser first before we can
             * determine how far we need to scroll for the animation
             */
            if (!lightbox.imgs_loaded)
                lightbox.load_image(mobile_slideshow[i], content);
            lb_content.append(mobile_slideshow[i]);
        }

        lightbox.caption_events();

        // leave this here if images have already been loaded
        if (lightbox.imgs_loaded) lightbox.do_animation(content);
    },

    replace_images: function() {
        var imgs, src, i = 0;

        imgs = $('#lightbox').find('img');

        for (i; i < imgs.length; i++) {
            src = $(imgs[i]).prop('src');
            $(imgs[i]).prop('src', src.replace('mobile', 'lightbox'));

            mobile_slideshow[i] =
                mobile_slideshow[i].replace('mobile', 'lightbox');
        }
    },

    caption_events: function() {
        var // click_handle = Modernizr.touch ? 'touchend' : 'click',
        mq_capability = Modernizr.mq('only all');

        $('.view-caption').on('click', function() {
            if (dragging) return;
            $(this).next('.caption').show();
            if (!mq_capability)
                $(this).next('.caption').find('h3,p').addClass('no-mq');
            $(this).hide();
        });
        $('span.close-area').on('click', function() {
            if (dragging) return;
            $(this).parents('.caption').hide();
            if (!mq_capability)
                $(this).parents('.caption').find('h3,p').removeClass('no-mq');
            $(this).parents().siblings('.view-caption').show();
        });
    },

    imgs_loaded: 0,

    load_image: function(obj, content) {
        var parsed_html = $.parseHTML(obj);
        $('<img>').load(function() {
            lightbox.imgs_loaded++;
            if (lightbox.imgs_loaded === content.idx)
                lightbox.do_animation(content);
        }).prop({
            'src': parsed_html[0].children[0].src
        });
    },

    do_animation: function(content) {
        var lb = $('#lightbox'),
            lb_item = $('#lightbox .owl-container .item').eq(content.idx);
        $('#lightbox').animate({

            /**
             * to get the offset of the child elem lb_item,
             * subtract the offset of the parent lb first
             */
            scrollTop: lb_item.offset().top - lb.offset().top - 70
        }, 1000);
    },

    check_answers: function() {
        var correct, selected_answer, selected,
            selected_parent, correct_parent, i, num_correct = 0;

        lightbox.disable_inputs();

        for (i = 0; i < quiz.items.length; i++) {
            correct = $('.question_' + i + '.choice_' +
                quiz.items[i].correct_answer);
            selected = $('#army-quiz input.question_' + i + ':checked');
            selected_answer = selected.val();
            selected_parent = selected.parents('.answer-container div');
            correct_parent = correct.parents('.answer-container div');

            if (selected_answer) {

                correct_parent.addClass('correct');
                correct.next('span').addClass('correct');

                if (parseInt(selected_answer, 10) !==
                    quiz.items[i].correct_answer) {

                    selected_parent.addClass('incorrect');
                    selected.next('span').addClass('incorrect');
                } else {
                    num_correct++;
                }
            } else {

                correct_parent.addClass('incorrect');
                correct.next('span').addClass('incorrect');
            }
        }
        lightbox.add_tweet(num_correct);
    },

    mark_all_correct: function() {
        var correct, correct_parent, i = 0;

        lightbox.disable_inputs();

        for (i; i < quiz.items.length; i++) {
            correct =
                $('.question_' + i + '.choice_' + quiz.items[i].correct_answer);
            correct_parent = correct.parents('.answer-container div');

            correct_parent.addClass('correct');
            correct.next('span').addClass('correct');
        }
        lightbox.add_tweet(quiz.items.length);
    },

    disable_inputs: function() {
        // var click_handle = Modernizr.touch ? 'touchend' : 'click';

        // prevent future clicking - and show reset button
        $('#army-quiz .answer-container').off('click');
        $('.submit-button div').off('click');
        $('.submit-button div').text('Reset Quiz').on('click', function() {
            if (dragging) return;
            lightbox.reset_quiz();
        });
        $('input').prop('disabled', true);
    },

    add_tweet: function(num_correct) {
        var tweet, heading = $('<h3>');

        tweet = encodeURIComponent(
            'I got ' + num_correct + ' out of ' + quiz.items.length +
            ' on the Army Birthday Quiz!'
        );
        $('.num-correct').show();
        $('.num-correct .y-container').empty().append(
            heading.text(
                'You got ' + num_correct + ' out of ' + quiz.items.length +
                ' on the Army Birthday Quiz! Tweet your score.'
            ),
            $('<a>').prop({
                'target': '_blank',
                'class': 'twitter-hashtag-button',
                'href': 'https://twitter.com/intent/tweet?button_hashtag=Arm' +
                    'yBDay&text=' +
                    tweet + '&url=http://www.army.mil/birthday/'
            }).text('Tweet #ArmyBDay')
        );
        if (typeof twttr !== 'undefined') twttr.widgets.load();
    },

    add_quiz: function() {
        var // click_handle = Modernizr.touch ? 'touchend' : 'click',
        header_content = $('.lightbox-header'),
            inner_content = $('#lightbox .inner-container'),
            question, answer, question_container, paragraphs, str,
            form = $('<form>').prop({
                'id': 'army-quiz'
            }),
            num_correct_div = $('<div>').prop({
                'class': 'num-correct'
            }),
            submit_button = $('<div>').prop({
                'class': 'submit-button'
            }),
            i = 0,
            j, x = 0; // set i to 0

        header_content.prepend(
            $('<div>').prop({
                'class': 'inner-wrap'
            }).append(
                $('<h2>').text('Test your army knowledge')
            )
        );

        num_correct_div.append(
            $('<div>').prop({
                'class': 'y-container'
            })
        );

        inner_content.prepend(
            $('<p>').prop({
                'class': 'intro-p'
            }).text('How Well Do You Know Your Nation\'s Army?')
        );

        for (i; i < quiz.items.length; i++) {
            question = $('<div>').prop({
                'class': 'question'
            }).text(quiz.items[i].question);

            question_container = $('<div>').prop({
                'class': 'question-container'
            }).append(question);

            for (j = 0; j < quiz.items[i].choices.length; j++) {
                answer = $('<label for="choice_' + j + '_' + i + '">' +
                    '<input type="radio" name="question_' + i + '" value="' +
                    j + '" class="question_' + i + ' choice_' + j +
                    '" id="choice_' + j + '_' + i + '" />' +
                    '<span></span></label><p>' + quiz.items[i].choices[j] +
                    '</p>');
                question_container.append(
                    $('<div>').prop({
                        'class': 'answer-container'
                    }).append(
                        $('<div>').html(answer)
                    )
                );
            }

            form.append(question_container);
        }

        form.append(num_correct_div);

        inner_content.append(
            form.append(
                submit_button.append(
                    $('<div>').text('Submit My Answers')
                )
            )
        );
        paragraphs = $('.answer-container p');
        for (x; x < paragraphs.length; x++) {

            /**
             * the idea here is to add the half-w-container
             * class to quiz questions
             * that don't need to be full width,
             * so if there are less than six words, add the class
             */
            str = $(paragraphs[x]).text();
            if (str.split(' ').length <= 4) {
                $(paragraphs[x])
                    .parents('.answer-container')
                    .addClass('half-w-container');
            }
        }

        submit_button.find('div').on('click', function() {
            if (dragging) return;
            lightbox.check_answers();
        });
        lightbox.quiz_events();
    },

    reset_quiz: function() {
        var answer_container = $('#army-quiz .answer-container div'),
            // click_handle = Modernizr.touch ? 'touchend' : 'click',
            spans = $('#army-quiz span'),
            submit_button = $('.submit-button'),
            num_correct_div = $('.num-correct');

        lightbox.quiz_events();
        $('input').prop({
            'checked': false,
            'disabled': false
        });

        $('#lightbox').animate({
            scrollTop: 0
        }, 1000);
        answer_container.removeClass();
        spans.removeClass();
        num_correct_div.hide();

        submit_button.find('div').off('click');
        submit_button.find('div').text('Submit My Answers')
            .on('click', function() {
                if (dragging) return;
                lightbox.check_answers();
            });
    },

    quiz_events: function() {
        // var click_handle = Modernizr.touch ? 'touchend' : 'click';
        $('#army-quiz .answer-container').on('click', function() {
            if (dragging) return;
            $(this).find('input').prop('checked', true);
            $(this).parent('.question-container')
                .find('.answer-container div, span')
                .removeClass();
            $(this).find('div, span').addClass('checked');
            return false;
        });
    },

    lb_close: function() {
        $('#lightbox').remove();
        $('html').css('overflow-y', 'scroll');
    },

    fix_header_width: function() {
        var outer_w = $('#lightbox .outer-container').width();
        if ($(window).width() <= 992 && $(window).width() > 699) {
            $('#lightbox .lightbox-header-wrap').width(outer_w);
        } else {
            $('#lightbox .lightbox-header-wrap').width('auto');
        }
    },

    bind_events: function() {
        // var click_handle = Modernizr.touch ? 'touchend' : 'click';

        $('.link').on('click', function(e) {
            e.stopPropagation();
            if (dragging) return;
            var idx = $(this).parent().hasClass('bday-images') ?
                $(this).index() :
                $(this).parent().index();

            lightbox.init();
            if (window_width > 992) {
                lightbox.add_slideshow({
                    'idx': idx
                });
            } else {
                lightbox.add_mobile_slideshow({
                    'idx': idx
                });
                lightbox.fix_header_width();
            }
        });

        $('.quiz').on('click', function() {
            if (dragging) return;
            lightbox.init();
            lightbox.add_quiz();
            lightbox.fix_header_width();
        });

        $(document).on('keyup', function(e) {
            var lb = $('#lightbox'),
                owl = $('#lightbox .owl-container').data('owlCarousel');
            if (lb.length) {
                if (e.keyCode === 27) {
                    lightbox.lb_close();
                } else if (e.keyCode === 39 && owl) {
                    owl.next();
                } else if (e.keyCode === 37 && owl) {
                    owl.prev();
                }
            }
        });
    }
};

products = {
    bind_events: function() {
        var link;

        $('.product-link, .product-link a').on('click', function(e) {
            link = e.target.tagName != 'A' ?
                $(this).find('a').prop('href') :
                $(this).prop('href');
            window.open(link);
            return false;
        });
    }
};

slider = {
    init: function() {
        var item, classname, lb_filename, folder,
            mq_capability = Modernizr.mq('only all'),
            i = 0; // set i to 0

        for (i; i < slideshow_details.items.length; i++) {
            classname = !mq_capability ? 'item no-mq' : 'item';
            lb_filename = slideshow_details.items[i].png_filename ?
                slideshow_details.items[i].png_filename :
                slideshow_details.items[i].filename;
            folder = window_width <= 480 ? 'mobile/' : 'lightbox/';

            // lightbox_slideshow obj used in lightbox.add_slideshow
            item = '<div class="' + classname + '">' +
                '<img class="lazyOwl" data-src="' + path + 'lightbox/' +
                lb_filename +
                '" alt="' + slideshow_details.items[i].alt + '"/>' +
                '</div>';
            lightbox_slideshow.push({
                'item': item
            });

            // mobile_slideshow obj used in lightbox.add_mobile_slideshow
            item = '<div class="' + classname + '">' +
                '<img src="' + path + folder + lb_filename +
                '" alt="' + slideshow_details.items[i].alt + '"/>' +
                '<div class="view-caption">View Caption</div>' +
                '<div class="caption">' +
                '<h3 class="img-title">' + slideshow_details.items[i].title +
                '</h3><p class="img-cutline">' +
                slideshow_details.items[i].cutline + '</p>' +
                '<p class="img-download"><a href="' + path + 'original/' +
                slideshow_details.items[i].filename +
                '" target="_blank">Download Original' +
                '<span class="download"></span></a>' +
                '<span class="close-area"><span class="close"></span></span>' +
                '</p>' +
                '</div><div class="clearboth"></div></div>';
            mobile_slideshow.push(item);

            // main_slideshow_items obj used on doc ready
            item = '<div class="item link">' +
                '<img class="lazyOwl" data-src="' + path + 'thumbnails/' +
                slideshow_details.items[i].filename +
                '" src="' + path + 'thumbnails/' +
                slideshow_details.items[i].filename +
                '" alt="' + slideshow_details.items[i].alt + '"/>' +
                '<div class="overlay"></div>' +
                '</div>';
            main_slideshow_items.owl.push({
                'item': item
            });
        }
        slider.add_side_images();
    },

    add_side_images: function() {
        var i = 0; // set i to 0

        for (i; i < main_slideshow_items.owl.length; i++) {
            $('.bday-images').append(main_slideshow_items.owl[i].item);
        }
        $('.bday-images .item:odd').addClass('odd');
    }
};

$(window).on('resize', function() {
    window_width = window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;

    news.story_box_size();
    menu.fix_placement();
    menu.adjust_flex_menu();

    if ($('#lightbox').length) {
        lightbox.fix_header_width();
        if (start_width <= 480 && start_width < window_width)
            lightbox.replace_images();
    }
});

$(window).on('scroll', function() {
    var bottom_offset, buffer,
        image_div = $('.no-ie.full-width-image-container').is(':visible') ?
            $('.no-ie.full-width-image-container') :
            $('.ie.full-width-image'),
        divs = [
            $('#intro'),
            $('#news'),
            $('#messages'),
            image_div
        ],
        i = 0,
        scroll_top = $(window).scrollTop();

    buffer = parseInt($('.top-menu').outerHeight(true), 10) +
        parseInt($('.fixed-menu').outerHeight(true), 10),

    menu.fix_placement();
    if (scroll_top > position && scroll_top > buffer) {
        // down
        $('.fixed-menu,.menu-list').addClass('collapsed').removeClass('show');
    } else {
        // up
        $('.fixed-menu,.menu-list').addClass('show').removeClass('collapsed');
    }
    position = scroll_top;

    for (i; i < divs.length; i++) {

        // 75 offset to make up for the margins plus a little extra
        if (scroll_top >= divs[i].offset().top - 75) {
            $('.primary-side-menu ul li').removeClass('active');
            $('.primary-side-menu ul li').eq(i).addClass('active');
        }
    }

    /**
     * bottom offset of 75 for the footer
     * aka when we scroll down to the top of the footer,
     * the if statement returns true
     * since the footer has a height of 75px
     */
    bottom_offset = $('.main').outerHeight(true) - 75;
    if (($(window).outerHeight(true) + scroll_top) >= bottom_offset &&
        window_width <= 1600) {
        $('.primary-side-menu ul li').removeClass('active');
        $('.primary-side-menu ul li:last').addClass('active');
    }
});

$(document).ready(function() {
    FastClick.attach(document.body);
    slider.init();

    var options = {
        jsonPath: main_slideshow_items,
        items: 4,
        itemsScaleDown: false,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: [768, 3],
        itemsMobile: [480, 2],
        pagination: false,
        navigation: true,
        navigationText: ['', ''],
        stopOnHover: true,
        addClassActive: true,
        lazyLoad: true,
        responsive: true
    };

    $('#slides').owlCarousel(options);

    $('#senior-leader-quotes').owlCarousel({
        singleItem: true,
        navigation: false,
        pagination: false,
        stopOnHover: true,
        autoPlay: true,
        responsive: true,
        transitionStyle: 'backSlide'
    });

    options.jsonPath = false;
    $('#product-slides').owlCarousel(options);

    if (Modernizr.touch) {
        $('body').on('touchmove', function() {
            dragging = true;
        })
            .on('touchstart', function() {
                dragging = false;
            });
    }

    menu.fix_placement();
    menu.adjust_flex_menu();
    menu.bind_events();
    hist.bind_events();
    lightbox.bind_events();
    products.bind_events();
    start_width = window_width;
});

// :)

var kkeys = [],
    konami = '38,38,40,40,37,39,37,39,66,65';

$(document).keydown(function(e) {

    kkeys.push(e.keyCode);

    if (kkeys.toString().indexOf(konami) >= 0) {
        lightbox.mark_all_correct();
        kkeys = [];
    }

});
